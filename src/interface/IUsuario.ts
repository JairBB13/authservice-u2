export default interface IUsario{
    id: number
    email: string
    name: string
    password: string
}