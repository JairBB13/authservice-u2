import { ref } from "vue"
import type { Ref } from "vue"
import type IUsario from "@/interface/IUsuario"

export default class ApiService{
    private usuarios:Ref<IUsario[]> 
    private usuario:Ref<IUsario>
    private url = 'http://172.16.107.120:3000'

    constructor(){
        this.usuarios = ref([])
        this.usuario = ref({}) as Ref<IUsario>
    }

    getUsuarios():Ref<IUsario[]> {
        return this.usuarios
    }

    getUsuario():Ref<IUsario>{
        return this.usuario
    }
    
    async obternerUsuarios(): Promise<void> {
        try {
            const respuesta = await fetch(this.url + '/Users', {
                headers: {
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*'
                  },
            })
            const resultado = await respuesta.json()
            console.log(resultado);
     
            this.usuarios.value = resultado
        } catch (error) {
            console.error('Error al obtener los usuarios' + error)
        }
   }

   async obtenerUsuario(email: string): Promise<void> {
        try {
            const respuesta = await fetch(this.url + '/User?' + email)
            const resultado: IUsario = await respuesta.json()
            console.log(resultado);
    
            this.usuario.value = resultado
        } catch (error) {
            console.error('Error al obtener el usuario' + error);
        }

   }
   
   async crearUsuario(email: string, name: string, password: string) {
        const respuesta = await fetch(this.url + '/Register?' + 'email=' + email + '&name=' + name + '&password=' + password, {
            method: 'POST'
        } )
        const resultado = await respuesta.json()
        console.log(resultado);
   }


}