import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import UsuariosVue from '@/views/Usuarios.vue'
import UsuarioVue from '@/views/Usuario.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/AboutView.vue')
    },
    {
      path: '/usuarios',
      name: 'usuarios',
      component: UsuariosVue
    },
    {
      path: '/usuario/:email',
      name: 'usuario',
      component: UsuarioVue
    }
  ]
})

export default router
